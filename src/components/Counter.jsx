import React, { Component } from 'react';

class Counter extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            counter : 0
        }
    }

    handleDecrease = () => {
        this.setState({
            counter : this.state.counter - 1
        })
    }

    handleIncrease = () => {
        this.setState({
            counter : this.state.counter + 1
        })
    }
    
    render() {
        return (
            <div>
                <h1>{this.state.counter}</h1>

                <button onClick={() => this.handleDecrease()}>Decrease</button>
                <button onClick={() => this.handleIncrease()}>Increase</button>

                
            </div>
        );
    }
}

export default Counter;
