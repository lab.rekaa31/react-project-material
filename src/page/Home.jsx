import React, { Component } from 'react';
import { Button } from 'react-bootstrap';

class Home extends Component {
    render() {
        return (
            <div>
                <h1>Home</h1>

                {/* <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="https://via.placeholder.com/1280x720" class="d-block w-100" alt="..." />
                        </div>
                        <div class="carousel-item">
                            <img src="https://via.placeholder.com/1280x600" class="d-block w-100" alt="..." />
                        </div>
                        <div class="carousel-item">
                            <img src={ImageOne} class="d-block w-100" alt="..." />
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div> */}
                {/* Native Way */}
                <button className='btn btn-primary'>Home</button>

                {/* Component Way */}
                <Button 
                    variant="primary"
                    active={true}>
                        Button #1
                </Button>
            </div>
        );
    }
}

export default Home;
